import SharedNavbar from './SharedNavbar/SharedNavbar.vue';
import SharedButton from './SharedButton/SharedButton.vue';
import Vue from 'vue';

export const components = {
	SharedNavbar,
	SharedButton
} as any;

Object.keys(components).forEach((name) => Vue.component(name, components[name]));
