import ViewLogin from '../views/ViewLogin.vue';

export const routes = [
	{
		path: '/',
		redirect: 'Home'
	},
	{
		path: '/home',
		name: 'Home',
		component: ViewLogin
	},
	{
		path: '/product',
		name: 'Product',
		// route level code-splitting
		// this generates a separate chunk (about.[hash].js) for this route
		// which is lazy-loaded when the route is visited.
		component: () =>
			import(/* webpackChunkName: "Product" */ '../views/ViewProduct.vue')
	},
	{
		path: '/search',
		name: 'Search',
		component: () => import(/* webpackChunkName: "Login" */ '../views/ViewSearch.vue')
	}
];
