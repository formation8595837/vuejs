import Vue from 'vue';
import Vuex from 'vuex';
import { Actions } from './action';
import { middleware } from '../middleware/globalMiddleware';

export { Actions };

Vue.use(Vuex);

export interface Notification {
	message: string;
	duration?: number;
	delay?: number;
}

export default new Vuex.Store({
	plugins: [middleware],
	state: {
		count: 0,
		products: [],
		notifications: 'No message'
	},
	getters: {},
	mutations: {
		[Actions.INCREMENT](state) {
			state.count++;
		},
		[Actions.APP_NOTIFY](state, message: string) {
			state.notifications = message;
		},
		[Actions.PRODUCTS_UPDATE](state, productList) {
			state.products = productList;
		}
	},
	//Une action entraîne une autre action ou une mutation
	actions: {
		[Actions.INCREMENT]() {
			this.commit(Actions.INCREMENT);
		},
		[Actions.APP_NOTIFY](context: any, payload: Notification) {
			const delayToStart = payload.delay ?? 0;
			const notificationDuration = payload.duration ?? 0;
			const notificationEndMessage = '';

			setTimeout(
				() => this.commit(Actions.APP_NOTIFY, payload.message),
				delayToStart
			);
			setTimeout(
				() => this.commit(Actions.APP_NOTIFY, notificationEndMessage),
				delayToStart + notificationDuration
			);
		},
		async [Actions.PRODUCTS_REQUEST](context: any): Promise<void> {
			const notification: Notification = {
				message: 'Recherche du produit',
				duration: 2000
			};
			this.dispatch(Actions.APP_NOTIFY, notification);
		},
		[Actions.PRODUCTS_UPDATE](context: any, payload: []) {
			const notification: Notification = {
				message: 'Produit trouvé',
				duration: 2000
			};
			this.commit(Actions.PRODUCTS_UPDATE, payload);
			this.dispatch(Actions.APP_NOTIFY, notification);
		}
	} as any,
	modules: {}
});
