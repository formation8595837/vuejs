const API_ENDPOINT = 'https://dummyjson.com/products';

async function fetchProducts() {
	return (await request(API_ENDPOINT)).products;
}

async function request(url: string) {
	return fetch(url).then((res) => res.json());
}

export { fetchProducts, request };
