import { fetchProducts } from '../services/productService';
import { Actions } from '../store/action';

export const middleware = (store: any) => {
	store.subscribeAction(async (action: any, state: any) => {
		if (action.type !== Actions.PRODUCTS_REQUEST) {
			return;
		}
		const products = await fetchProducts();
		store.dispatch(Actions.PRODUCTS_UPDATE, products);
	});
};
